# -*- coding: utf-8 -*-
"""
Did you already knew about the python logging module?
Docs: https://docs.python.org/3/howto/logging.html:
"""
import logging


# =============================================================================
# Configure the logger.
# =============================================================================

# Set the level written in capitals. The default is warning.:
#logging.basicConfig(level=logging.DEBUG)

# Format the standard output style (severity:logger name:message):
#logging.basicConfig(format='%(levelname)s: %(message)s')
#logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p') # Display the date.

# Log to a file.
#logging.basicConfig(filename='logfile.log', filemode='w', level=logging.DEBUG)

# =============================================================================
# Events and Importance
# =============================================================================

# Logging is a means of tracking events that happen when some software runs.
# Events have an importance level and each level has a corresponding function.

logging.debug('This is just for debugging.') #  For status monitoring or fault investigation.
logging.info('This is an info.') #  Confirm that things are working as expected.
logging.warning('This is a warning.') # Issue a warning.
# Raise an exception -> error regarding a particular runtime event
# Report suppression of an error without raising exceptions.
logging.error('This is a serious problem.')
#logging.exception('This is an exception.') #  Dumps a stack trace along with it.
logging.critical('This is a critical error.') #  The program itself may be unable to continue running.

print('Hello World') #  Display output. (Not for tracking events.)
