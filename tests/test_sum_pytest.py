import pytest

class TestSum():

    def test_sum(self):
        assert sum([1, 2, 3]) == 6
   
    def test_sum_tuple(self):
        assert sum((1, 2, 4)) == 6

if __name__ == '__main__':
    pytest.main()
